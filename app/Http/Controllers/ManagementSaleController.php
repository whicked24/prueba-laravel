<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ManagementSaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $toInvoice = Sale::whereNull('invoiced')->get();

        $invoiced = Sale::whereNotNull('invoiced')->get();

        return view('sale.management-sale',compact('toInvoice','invoiced'));
    }

    public function store(Request $request)
    {
        $invoice = Sale::find($request->invoice);
        $invoice->update([
            	'invoiced' => Carbon::now(),
                'invoiced_by' => $request->user()->id,
        ]);

        return redirect('management-sale')->with('menssage', 'factura emitida!');
    }

    public function show(Sale $managementSale)
    {
        return view('sale.detail-sale',compact('managementSale'));
    }
}
