<?php

namespace App\Models;

use Illuminate\Support\Str;
use Silber\Bouncer\Database\Role as BounceRole;
use App\Transformers\RoleTransformer;

class Role extends BounceRole
{
    const TYPE_ADMIN = 'admin';
    const TYPE_USER = 'user';

    public $transformer = RoleTransformer::class;

    protected $fillable = [
        'name',
        'title',
        'type',
        'estatus',
        'user_limit',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::lower($value);
        $this->attributes['title'] = $value;
    }

    static public function findByName($value)
    {
        return static::where('name', $value)->firstOrFail();
    }

    public function scopeFilter($query, $filters)
    {
        if(isset($filters['estatus'])){
            $query->where('estatus', '=', $filters['estatus']);
        }
    }

    static public function limitReached($value)
    {
        $role = static::findByName($value);

        if (is_null($role->user_limit)) {
            return true;
        }

        $users = User::with('roles')->whereIs($value)->count();

        return $users < $role->user_limit;
    }
}
