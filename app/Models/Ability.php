<?php

namespace App\Models;

use Illuminate\Support\Str;
use Silber\Bouncer\Database\Ability as BouncerAbility;
use App\Transformers\AbilityTransformer;

class Ability extends BouncerAbility
{
    public $transformer = AbilityTransformer::class;

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value === '*' ? $value : Str::slug(Str::lower($value), '-');
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = Str::title($value);
    }

    public function childs()
    {
        return $this->hasMany(Ability::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Ability::class, 'parent_id');
    }

    public function scopeFilter($query, $filters)
    {
        if(isset($filters['estatus'])){
            $query->where('estatus', '=', $filters['estatus']);
        }
    }
}
