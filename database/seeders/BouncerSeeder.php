<?php

namespace Database\Seeders;

use Silber\Bouncer\BouncerFacade as Bouncer;
use Illuminate\Database\Seeder;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow('developer')->everything();

        // Abilities
        // Dashboard
        $dashboard = Bouncer::ability()->create(['name' => 'tablero', 'title' => 'tablero']);
        // Management Users
        $profile = Bouncer::ability()->create(['name' => 'mi perfil', 'title' => 'mi perfil']);
        $userManagement = Bouncer::ability()->create(['name' => 'gestion de usuarios', 'title' => 'gestion de usuarios']);
        // Roles
        $roles = Bouncer::ability()->create(['name' => 'perfiles', 'title' => 'perfiles', 'parent_id' => $userManagement->id]);
        $createRole = Bouncer::ability()->create(['name' => 'crear perfil', 'title' => 'crear perfil', 'parent_id' => $userManagement->id]);
        $editRole = Bouncer::ability()->create(['name' => 'editar perfil', 'title' => 'editar perfil', 'parent_id' => $userManagement->id]);
        $deleteRole = Bouncer::ability()->create(['name' => 'eliminar perfil', 'title' => 'eliminar perfil', 'parent_id' => $userManagement->id]);
        // Users
        $users = Bouncer::ability()->create(['name' => 'usuarios', 'title' => 'usuarios', 'parent_id' => $userManagement->id]);
        $createUser = Bouncer::ability()->create(['name' => 'crear usuario', 'title' => 'crear usuario', 'parent_id' => $userManagement->id]);
        $editUser = Bouncer::ability()->create(['name' => 'editar usuario', 'title' => 'editar usuario', 'parent_id' => $userManagement->id]);
        $deleteUser = Bouncer::ability()->create(['name' => 'eliminar usuario', 'title' => 'eliminar usuario', 'parent_id' => $userManagement->id]);
        // Abilities
        $abilities = Bouncer::ability()->create(['name' => 'privilegios', 'title' => 'privilegios', 'parent_id' => $userManagement->id]);
        $createAbility = Bouncer::ability()->create(['name' => 'crear privilegio', 'title' => 'crear privilegio', 'parent_id' => $userManagement->id]);
        $editAbility = Bouncer::ability()->create(['name' => 'editar privilegio', 'title' => 'editar privilegio', 'parent_id' => $userManagement->id]);
        $deleteAbility = Bouncer::ability()->create(['name' => 'eliminar privilegio', 'title' => 'eliminar privilegio', 'parent_id' => $userManagement->id]);
        
        $client = Bouncer::role()->create(['name' => 'client', 'title' => 'client']);
        Bouncer::allow($client)->to([
            $dashboard,
            $profile,
        ]);

        $admin = Bouncer::role()->create(['name' => 'admin', 'title' => 'admin']);
        Bouncer::allow($admin)->to([
            $dashboard,
            $profile,
        ]);
    }
}
