<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = bcrypt('123456As.');
        $user = User::create([
            'name' => 'jonan',
            'email' => 'whicked24@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => $pass,
        ]);

        $user->assign('developer');

        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@yopmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => $pass,
        ]);

        $admin->assign('admin');

        $client = User::create([
            'name' => 'client',
            'email' => 'client@yopmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => $pass,
        ]);

        $client->assign('client');
    }
}
