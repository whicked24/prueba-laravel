@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    <div class="row justify-content-center">
    <table class="table">
        <thead>
            <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Impuesto</th>
            <th>--</th>
            </tr>
        </thead>
        <tbody>
            @foreach($product as $products)
            <tr>
                <td>{{ $products->id }}</td>
                <td>{{ $products->name }}</td>
                <td>{{ $products->price }}</td>
                <td>{{ $products->tax }}</td>
                <td>
                    <form action="{{route('sale.store')}}" method="post">
                    @csrf
                    <input type="hidden" value="{{$products->id}}" name="producto">
                    <button class="btn btn-success btn-sm btn-round">Comprar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection
