@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    <div class="row justify-content-center">
        <h5>Facturas Pendientes</h5>
    <table class="table">
        <thead>
            <tr>
            <th>#</th>
            <th>Cliente</th>
            <th>Producto</th>
            <th>Precio</th> 
            <th>--</th>
            </tr>
        </thead>
        <tbody>
            @foreach($toInvoice as $invoice)
            <tr>
                <td>{{ $invoice->id }}</td>
                <td>{{ $invoice->user->name }}</td>
                <td>{{ $invoice->product->name }}</td>
                <td>{{ $invoice->product->price }}</td>
                <td>
                    <form action="{{route('management-sale.store')}}" method="post">
                    @csrf
                    <input type="hidden" value="{{$invoice->id}}" name="invoice">
                    <button class="btn btn-success btn-sm btn-round">Facturar..</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <h5>Facturas Emitidas</h5>
    <table class="table">
        <thead>
            <tr>
            <th>#</th>
            <th>Cliente</th>
            <th>Producto</th>
            <th>Precio</th> 
            <th>--</th>
            </tr>
        </thead>
        <tbody>
            @foreach($invoiced as $invoice)
            <tr>
                <td>{{ $invoice->id }}</td>
                <td>{{ $invoice->user->name }}</td>
                <td>{{ $invoice->product->name }}</td>
                <td>{{ $invoice->product->price }}</td>
                <td>
                    <form action="{{route('management-sale.show',$invoice->id)}}" method="GET">
                    @csrf
                    <button class="btn btn-success btn-sm btn-round">Ver Detalle..</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection
