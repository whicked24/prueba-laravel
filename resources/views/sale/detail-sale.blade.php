@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
        <div class="row justify-content-center">
            <div class="card text-center">
                <div class="card-header">
                    Factura # {{$managementSale->id}}
                </div>
                <div class="card-body">
                    <h5 class="card-title">Cleinte: {{$managementSale->user->name}}</h5>
                    <p class="card-text">Producto : {{$managementSale->product->name}}</p>
                    <p class="card-text">Precio : {{ $managementSale->product->price }} </p>
                    <p class="card-text">Impuesto : {{ $managementSale->product->tax }}</p>
                    <p class="card-text">Total : {{ ($managementSale->product->price * $managementSale->product->tax)/100 +  $managementSale->product->price }}</p>
                </div>
                <div class="card-footer text-muted">
                    {{  date('d/m/Y') }}
            </div>
        </div>
    </div>
</div>
@endsection
