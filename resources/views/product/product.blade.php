@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    <div class="row justify-content-center">
        <a class="btn btn-primary btn-large" href="{{route('product.create')}}">Agregar</a>
        <br>
    <table class="table">
        <thead>
            <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Impuesto</th>
            <th>--</th>
            </tr>
        </thead>
        <tbody>
            @foreach($product as $products)
            <tr>
                <td>{{ $products->id }}</td>
                <td>{{ $products->name }}</td>
                <td>{{ $products->price }}</td>
                <td>{{ $products->tax }}</td>
                <td>
                    <form action="{{route('product.edit', $products->id)}}">
                    @csrf
 
                    <button class="btn btn-warning btn-sm btn-round">Editar</button>
                    </form>
                    
                    <form action="{{route('product.destroy', $products->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                        <button class="btn btn-danger btn-sm btn-round">Eliminar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection
