@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
    <div class="row justify-content-center">
        <h5>Nuevo Producto</h5>
        
        @if(isset($product))
        <form method="POST" action="{{route('product.update', $product->id)}}">
        @csrf 
        @method('PUT')
        @else
        <form method="POST" action="{{route('product.store')}}">    
        @csrf
        @method('POST')
        @endif

        <div class="form-group">
            <label for="producto">Nombre del Producto</label>
            <input type="text" class="form-control" id="nombre"  name="name" placeholder="Ingrese el nombre."
            @isset($product) value="{{$product->name}}" @endisset
            >
        </div>
        <div class="form-group">
            <label for="precio">Precio</label>
            <input type="text" class="form-control" id="precio" placeholder="Precio" name="price"
            @isset($product) value="{{$product->price}}" @endisset
            >
        </div>
        <div class="form-group">
            <label for="impuesto">Impuesto</label>
            <input type="text" class="form-control" id="impuesto" placeholder="Impuesto" name="tax"
            @isset($product) value="{{$product->tax}}" @endisset
            >
        </div>
        <button type="submit" class="btn btn-primary btn-large">Submit</button>
    </form>
    </div>
</div>
@endsection
